# SPDX-License-Identifier: EFL-2.0
# Copyright (c) 2021 Max Rees
# See LICENSE for more information.

function seen_get(nick) {
	if (!(nick in SEEN)) {
		irc_say("I've never seen them before.")
		return
	}

	irc_say(util_age(systime() - SEEN[nick])" ago")
}

function seen_restore_db(        record, recordv) {
	log_warning("Restoring seen database...")
	delete SEEN
	while ((getline record < SEEN_FILE) == 1) {
		split(record, recordv, " ")
		if (!recordv[1] || !recordv[2])
			continue
		SEEN[recordv[1]] = recordv[2]
	}
	close(SEEN_FILE)
}

function seen_save_db(        nick) {
	log_warning("Saving seen database...")
	for (nick in SEEN)
		print nick" "SEEN[nick] > SEEN_FILE
	close(SEEN_FILE)
	SEEN_LAST_SAVE = systime()
}

BEGIN {
	HELP["seen"] = CMD_PREFIX"seen NICK: display time since NICK last spoke anywhere visible to the bot"
	seen_restore_db()
}

irc_nick && irc_msg {
	_seen_nick = irc_tolower(irc_nick)
	SEEN[_seen_nick] = systime()
	if (SEEN[_seen_nick] - SEEN_LAST_SAVE > SEEN_SAVE_INTERVAL)
		seen_save_db()
}

irc_cmd == "seen" {
	seen_get(irc_tolower(irc_msgv[2]))
	next
}

END {
	seen_save_db()
}
