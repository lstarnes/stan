# SPDX-License-Identifier: EFL-2.0
# Copyright (c) 2019-2021 Max Rees
# See LICENSE for more information.

BEGIN {
	HELP["status"] = CMD_PREFIX"status: show bot status information"
	STATUS_BIRTH = systime()
}

irc_cmd == "status" {
	irc_say("Child #"CHILD": "util_age(systime() - STATUS_BIRTH)" old with "NR" messages read")
	next
}
